//
//  ViewController.m
//  FT Index
//
//  Created by 皂亚明 on 2018/5/17.
//  Copyright © 2018年 QC. All rights reserved.
//
#define KStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeigth [UIScreen mainScreen].bounds.size.height

//#define IS_IPHONE_X ([UIScreen mainScreen].bounds.size.height >= 812)
//#define navStatusBar_X (IS_IPHONE_X?44:20)

#import "ViewController.h"

@interface ViewController ()<UIWebViewDelegate>
{
    UIWebView *_webView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSString *sysVersion = [[UIDevice currentDevice]systemVersion];
    _webView = [[UIWebView alloc]init];
    if (sysVersion.doubleValue >= 11.0) {
        _webView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeigth);
    }else{
        _webView.frame = CGRectMake(0, KStatusBarHeight, ScreenWidth, ScreenHeigth-KStatusBarHeight);
    }
    
    
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.bounces = NO;
    _webView.scalesPageToFit = YES;
    [self.view addSubview:_webView];
    _webView.delegate = self;
    [self setStatusBarBackgroundColor:[UIColor whiteColor]];
    NSString *devUrlStr = @"https://crmwww-dev.ga096.cn";
    NSString *onlineUrlStr = @"https://www.fthook.com";
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[date timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%.0f", a];
    NSString *urlStr = [NSString stringWithFormat:@"%@?v=%@&from=app&channelId=%@",onlineUrlStr,timeString,@"10000"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    
    
    UISwipeGestureRecognizer *backrecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(gobackPage)];
    [backrecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:backrecognizer];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(frontPage)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:recognizer];
    
    UILongPressGestureRecognizer* longPressed = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    longPressed.minimumPressDuration = 0.3;
    [_webView addGestureRecognizer:longPressed];
}

- (void)longPressed:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    
    CGPoint touchPoint = [recognizer locationInView:_webView];
    // 获取手势所在图片的URL，js中图片的地址是用src引用的
    NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", touchPoint.x, touchPoint.y];
    NSString *urlToSave = [_webView stringByEvaluatingJavaScriptFromString:imgURL];
    
    if (urlToSave.length == 0) {
        return;
    }
    
    [self showImageOptionsWithUrl:urlToSave];
}
- (void)showImageOptionsWithUrl:(NSString *)imgURL
{
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgURL]];
    UIImage* image = [UIImage imageWithData:data];
    
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    NSString *alertStr;
    if (error){
        alertStr= @"图片保存失败";
    }else {
        // 保存图片到手机
        alertStr= @"图片已保存至相册";
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    // 取消
    UIAlertAction *cancell = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancell];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)gobackPage
{
    [_webView goBack];
}
-(void)frontPage
{
    [_webView goForward];
}
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    NSURL * url = request.URL;
//    NSString * url_scheme = [url scheme];
//    NSString * url_host = [url host];
//    NSString * urlStr = [NSString stringWithFormat:@"%@",request.URL];
//    urlStr =[urlStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//    return YES;
//}
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"-------%@",error);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
