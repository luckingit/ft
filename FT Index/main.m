//
//  main.m
//  FT Index
//
//  Created by 皂亚明 on 2018/5/17.
//  Copyright © 2018年 QC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
