//
//  AppDelegate.h
//  FT Index
//
//  Created by 皂亚明 on 2018/5/17.
//  Copyright © 2018年 QC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

